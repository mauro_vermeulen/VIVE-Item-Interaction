﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public float minMargin, maxMargin;

    [SerializeField]
    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }

    [SerializeField]
    private float yAxis;

    [SerializeField]
    private float speed;

    private SteamVR_TrackedObject trackedObj;
    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    private bool startCycle;

    // Use this for initialization
    void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller == null)
        {
            Debug.Log("Controller not initialized.");
            return;
        }
        yAxis = controller.velocity.y;
        StartCycle();
    }

    //Checks if yAxis of controller is outside of the set margin, true if so.
    public bool CheckMargin(float yAxis)
    {
        if (yAxis < minMargin || yAxis > maxMargin)
            return true;
        else
            return false;
    }

    public void StartCycle()
    {
        //Starts walk cycle if outside margin and hair trigger is held in.
        if (CheckMargin(yAxis) && controller.GetHairTrigger())
        {
            speed = controller.velocity.y;
            if (speed < 0)
            {
                speed *= -1;
            }
            startCycle = true;
        }

        //Holds the current speed if inside the margin and if cyle has been started.
        else if (!CheckMargin(yAxis) && controller.GetHairTrigger() && startCycle == true)
        {
            speed = controller.velocity.y;
            if (speed < 0)
            {
                speed *= -1;
            }
        }

        //Stops speed if hair trigger is lifted up and inside margin.
        else if (!controller.GetHairTrigger() && !CheckMargin(yAxis))
        {
            startCycle = false;
            speed = Mathf.Lerp(speed, 0f, Time.time * .1f);

            if (speed < 0.1f)
                speed = 0f;
        }
    }

    public bool GetGripControllerDown()
    {
        if (controller.GetPressDown(gripButton))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool GetGripControllerUp()
    {
        if (controller.GetPressUp(gripButton))
            return true;
        else
            return false;
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }
}