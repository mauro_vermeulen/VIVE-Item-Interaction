﻿using UnityEngine;
using System.Collections;

public class InteractableItem : MonoBehaviour {

    public PickupController attachedController;

    private Rigidbody rb;
    private bool currentlyInteracting;
    private Transform interactionPoint;

    private float velocityFactor = 20000f;
    public Vector3 posDelta;

    private float rotationFactor = 400f;
    private Quaternion rotationDelta;
    private float angle;
    private Vector3 axis;
    

    // Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        interactionPoint = new GameObject().transform;
        velocityFactor /= rb.mass;
        rotationFactor /= rb.mass;
    }
	
	// Update is called once per frame
	void Update () {
        if (attachedController != null && currentlyInteracting)
        {
            posDelta = attachedController.transform.position - interactionPoint.position;
            rb.velocity = posDelta * velocityFactor * Time.fixedDeltaTime;

            rotationDelta = attachedController.transform.rotation * Quaternion.Inverse(interactionPoint.rotation);
            rotationDelta.ToAngleAxis(out angle, out axis);

            if (angle > 180)
            {
                angle -= 360;
            }

            if (angle < 5f)
            {
                rb.angularVelocity = Vector3.zero;
            }
            else
            {
                rb.angularVelocity = (Time.fixedDeltaTime * angle * axis) * rotationFactor;
            }

        }

	}

    public void BeginInteraction(PickupController controller)
    {
        attachedController = controller;
        interactionPoint.position = controller.transform.position;
        interactionPoint.rotation = controller.transform.rotation;
        interactionPoint.SetParent(transform, true);

        currentlyInteracting = true;
    }

    public void EndInteraction(PickupController controller)
    {
        if(controller == attachedController)
        {
            attachedController = null;
            currentlyInteracting = false;
        }
    }

    public bool IsInteracting()
    {
        return currentlyInteracting;
    }
}
