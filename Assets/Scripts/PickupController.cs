﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickupController : MonoBehaviour {

    HashSet<InteractableItem> objectsHoveringOver = new HashSet<InteractableItem>();

    private InputController ic;
    public InteractableItem closestItem;
    public InteractableItem interactingItem;

	// Use this for initialization
	void Start () {
        ic = GetComponent<InputController>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(ic.GetGripControllerDown())
        {
            float minDistance = float.MaxValue;

            float distance;
            foreach(InteractableItem item in objectsHoveringOver)
            {
                distance = (item.transform.position - transform.position).sqrMagnitude;

                if(distance < minDistance)
                {
                    minDistance = distance;
                    closestItem = item;
                }
            }

            interactingItem = closestItem;
            closestItem = null;

            if (interactingItem != null)
            {
                if (interactingItem.IsInteracting())
                {
                    interactingItem.EndInteraction(this);
                }

                interactingItem.BeginInteraction(this);
            }
        }

        if(ic.GetGripControllerUp() && interactingItem != null)
        {
            interactingItem.EndInteraction(this);
        }
	}

    private void OnTriggerEnter(Collider collider)
    {
        InteractableItem collidedItem = collider.GetComponent<InteractableItem>();
        if (collidedItem)
        {
            objectsHoveringOver.Add(collidedItem);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        InteractableItem collidedItem = collider.GetComponent<InteractableItem>();
        if (collidedItem)
            objectsHoveringOver.Remove(collidedItem);
    }
}
