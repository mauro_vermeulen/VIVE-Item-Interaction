﻿using UnityEngine;
using System.Collections;

public class InteractibleItem : MonoBehaviour {

    public Rigidbody Rigidbody;
    public BoxCollider collider;
    private bool currentlyInteracting;
    public Controller attachedHand;
    private Transform interactionPoint;
    public Transform specificInteractionoint;
    public bool isToBeReleased;

    public string itemTag;

    public Transform rotationNutral;

    // Use this for initialization
    void Start()
    {
        Rigidbody = this.GetComponent<Rigidbody>();
        collider = this.GetComponent<BoxCollider>();
        interactionPoint = new GameObject().transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (attachedHand && currentlyInteracting)
        {
            float angle;
            Vector3 axis;

            Quaternion rotationDelta;
            Vector3 posDelta;

            collider.isTrigger = true;

            if (specificInteractionoint != null)
            {
                rotationDelta = attachedHand.transform.rotation * Quaternion.Inverse(specificInteractionoint.rotation);
                posDelta = (attachedHand.transform.position - specificInteractionoint.position);
            }

            else
            {
                rotationDelta = attachedHand.transform.rotation * Quaternion.Inverse(this.transform.rotation);
                posDelta = (attachedHand.transform.position - this.transform.position);
            }

            rotationDelta.ToAngleAxis(out angle, out axis);

            if (angle > 180)
            {
                angle -= 360;
            }

            if (angle != 0)
            {
                Vector3 AngularTarget = angle * axis;
                this.Rigidbody.angularVelocity = Vector3.MoveTowards(this.Rigidbody.angularVelocity, AngularTarget, 10f);
            }

            Vector3 VelocityTarget = posDelta / Time.fixedDeltaTime;
            this.Rigidbody.velocity = Vector3.MoveTowards(attachedHand.rb.velocity, VelocityTarget, 10f);
        }
    }

    public void BeginInteraction(Controller wand)
    {
        itemTag = this.gameObject.tag;

        this.Rigidbody.isKinematic = false;
        attachedHand = wand;
        interactionPoint.position = wand.transform.position;
        interactionPoint.rotation = wand.transform.rotation;
        interactionPoint.SetParent(transform, true);

        currentlyInteracting = true;
        rotationNutral = this.transform;

     // attachedHand.model.enabled = false;
    }

    public void EndInteraction(Controller wand)
    {
        if (wand == attachedHand && !isToBeReleased)
        {
         // attachedHand.model.enabled = true;
            itemTag = " ";
            attachedHand = null;
            currentlyInteracting = false;
            collider.isTrigger = false;
        }
    }

    public bool IsInteracting()
    {
        return currentlyInteracting;
    }
}


